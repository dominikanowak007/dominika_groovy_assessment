package com.example.dnowak_groovy.controlservice

import com.example.dnowak_groovy.networkbeans.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@RestController
class NetworkController {


    @Autowired
    private NetworkService networkService

    @GetMapping("/greeting")
    public String greeting() {
        return "************ WELCOME TO NETWORK GROOVY APPLICATION ************"
    }

    @GetMapping("/network/all")
     String getAllCarriers() {
        return networkService.getCurrentNetworkList()?.toString()
    }

    @GetMapping("/network/all/names")
    String getAllCarriersNames() {
        return networkService.getAllCarriersNames()?.toString()
    }

    @GetMapping("/gethubs/{carriername}")
    public String getAllHubsByCarrierName(@PathVariable String carriername) {
        return networkService.getAllHubsForCarrier(carriername)?.toString()
    }


  @GetMapping("/getnodes/{hubId}")
  public String getAllNodesByHubId(@PathVariable int hubId) {
      return networkService.getAllNodesByHubId(hubId)?.toString()
  }

  @GetMapping("/getnodebyid/{nodeId}")
  public String getNodeById(@PathVariable int nodeId)
  {
      return networkService.getNodeByNetworkId(nodeId).toString()
  }

  @GetMapping("/getnodebyname/{nodeName}")
  public String getNodeByName(@PathVariable String nodeName)
  {
       return   networkService.getNodebyName(nodeName)?.toString()

  }

  @GetMapping("/gethubbyname/{hubName}")
  public String getHubByName(@PathVariable String hubName)
  {
          return  networkService.getHubByName(hubName)?.toString()

  }
    /*************************************************************************  NETWORK NEW ELEMENTS SECTION *********************************************************************/

    @PostMapping("/addNode/{hubid}/newNode")
    ResponseEntity<?> addNodeToHubId(@PathVariable int hubid, @RequestBody NetworkNode newNode)
    {

        if (!networkService.identifierExistsOnNetwork(newNode.getNodeId()) && !networkService.nodeExistsOnHub(hubid, newNode.getNodeName())) {

            NetworkNode nodeCreated = networkService.addNodeToHubId(hubid, newNode.getNodeName(), newNode.getNodeId())

            if (nodeCreated == null) {
                return ResponseEntity.noContent().build()
            }

            URI location = ServletUriComponentsBuilder.fromPath(
                    "network/nodes/{nodeId}").buildAndExpand(nodeCreated.getNodeId()).toUri()

            return ResponseEntity.created(location).build()
        }
        else
        {
            return ResponseEntity.unprocessableEntity().header("Already Exists", "").body("Node with name: " +
                    newNode.getNodeName() + " for hub with id: " + hubid.toString()+
                    " or given identifier: " + newNode.getNodeId() + " already exists on the Network")
        }

    }

   @PostMapping("/network/add/{carrierName}/newHub")
      ResponseEntity<?> addHubToNamedCarrier(@PathVariable String carrierName, @RequestBody NetworkHub newHub) {
          if (networkService.carrierExistsOnNetwork(carrierName))
          {

              if (!networkService.identifierExistsOnNetwork(newHub.getHubId()) && !networkService.hubExistsforCarrier(carrierName, newHub.getHubName()))
              {

                  NetworkHub hubCreated = networkService.addHubToNamedCarrier(carrierName, newHub.getHubName(), newHub.getHubId())

                  if (hubCreated == null) {
                      return ResponseEntity.noContent().build()
                  }

                  URI location = ServletUriComponentsBuilder.fromPath(
                          "/network/hubNodes/{hubId}").buildAndExpand(hubCreated.getHubId()).toUri()

                  return ResponseEntity.created(location).build()

              } else {
                  return ResponseEntity.unprocessableEntity().header("Already Exists", "").body(" Identifier: " + newHub.getHubId() +
                          " or hub for carrier: "+ carrierName + " with name: " + newHub.getHubName() +
                          " already exists on the Network")
              }
          } else {
              return ResponseEntity.unprocessableEntity().header("Does Not Exist", "").body(" Carrier with name: " + carrierName + " does NOT exists on the Network")

          }

      }

        @PostMapping("/network/addCarrier/newNetworkCarrier")
        ResponseEntity<?> addCarrierToNetwork(@RequestBody NetworkCarrier newCarrier) {

            if (!networkService.carrierExistsOnNetwork(newCarrier.getCarrierName())) {
                NetworkCarrier carrierCreated = networkService.addCarrierToNetwork(newCarrier.getCarrierName())

                if (carrierCreated == null) {
                    return ResponseEntity.noContent().build()
                }

                URI location = ServletUriComponentsBuilder.fromPath(
                        "network/all").buildAndExpand().toUri()

                return ResponseEntity.created(location).build()
            } else
                return ResponseEntity.unprocessableEntity().header("Already Exists", "").body("Carrier with  name " + newCarrier.getCarrierName() + " already exists on the Network")

        }

        @PostMapping("/addAlarm/{networkIdentifier}/newAlarm")
        String addAlarmToNetwork(@PathVariable int networkIdentifier, @RequestBody NetworkAlarm newAlarm) {
           int result= networkService.addAlarmtoNetworkElement(networkIdentifier, newAlarm)

            if (result==0 ){
                return ResponseEntity.unprocessableEntity().header("Does Not Exist", "").body("Identifier: " + networkIdentifier + " does not exist on the Network")

        }
            else return "Alarm created!"
        }
    /************************************************************************* UPDATE SECTION *********************************************************************/



    @PatchMapping("/updateCarrier/{carrierName}/updatedCarrier")

     String updateCarrier(@PathVariable String carrierName, @RequestBody NetworkCarrier updatedCarrier){

        if (networkService.carrierExistsOnNetwork(carrierName))
        {
          boolean updated = networkService.updateCarrierName(carrierName,updatedCarrier.getCarrierName())
            return 'Carrier Updated: ' + (updated? "yes":" no")
        }
        else return'No carrier with name: '+ carrierName + ' exists on Network'
    }

    @PatchMapping("/updateHub/updatedHub")
     String updateHubName(@RequestBody NetworkHub updatedHub)
    {
        if (networkService.hubExists(updatedHub.getHubId()))
        {
            boolean updated = networkService.updateExistingHub(updatedHub.getHubId(), updatedHub.getHubName())
            return 'Hub Updated: ' + (updated? "yes":" no")
        }
        else return "Hub with the id "+ updatedHub.getHubId() +" does not exist "

    }

    @PatchMapping("/updateNodeName/properties")
     String updateNodeName( @RequestBody Map<String, String>  updates)
    {
        int nodeId = updates['nodeId'].toInteger()
        if (networkService.nodeExists(nodeId))
        {
           boolean updated =  networkService.updateNodeName(updates)
            return 'Node Updated: ' + (updated? "yes":" no")
        }

        else return "Node with the id "+ updates['nodeId'] +" does not exist "
    }


    /***********************************************************************************  DELETE SECTION   **********************************/


    @DeleteMapping ("/remove/{Id}")

    public String deleteNetworkItemById(@PathVariable int Id)
    {
        if(networkService.hubExists(Id)) {
          boolean deleted =  networkService.deleteHubFromNetwork(Id)
            return "Hub with id: " + Id.toString()+ " deleted: " + (deleted? " yes":" no")
        }
        else if(networkService.nodeExists(Id)) {
           boolean deleted = networkService.deleteNodeFromNetwork(Id)
            return "Node with id: " + Id.toString()+ " deleted: "+ (deleted? " yes":" no")
        }
        else "No Network Element with id: "+ Id +" exists !"
    }


    @DeleteMapping ("/network/removeCarrier/{carrierName}")
    String deleteNetworkCarrier(@PathVariable String carrierName)
    {
        if(networkService.carrierExistsOnNetwork(carrierName))
        {
           boolean deleted =  networkService.deleteCarrier(carrierName)
            return "Carrier: " + carrierName+ " deleted: "+ (deleted? " yes":" no")
        }
        else return  "No Carrier with name: "+ carrierName +" exists on Network!"

    }

    @DeleteMapping ("/removeAllAlarms/{id}")
     String deleteAlarmsFromNetworkElement(@PathVariable int id)
    {
        if(networkService.identifierExistsOnNetwork(id))
        {
            if(networkService.hubExists(id))
            {
                Boolean alarmDeleted = networkService.removeAllAlarmsFromHub(id)
                return "Hub Alarms"+ (alarmDeleted ? "" : " NOT")  + " Deleted"

            }
            else if (networkService.nodeExists(id))
            {
                Boolean alarmDeleted = networkService.removeAllAlarmsFromNode(id)
                return "Node Alarms"+ (alarmDeleted ? "" : " NOT")  + " Deleted"
            }
        }

        return "Item with the Identifier: " + id +  " does NOT exist on Network!"
    }

    @DeleteMapping ("/removeAlarmType/{id}/{alarmType}")
    String deleteAlarmTypeFromNetworkElement(@PathVariable int id, @PathVariable String alarmType)
    {

        if(networkService.identifierExistsOnNetwork(id))
        {
            def alarm = AlarmTypeEnum.valueOf(alarmType)
            if(networkService.hubExists(id))
            {

                Boolean alarmDeleted = networkService.removeAlarmFromHub(id, alarm)
                return "Hub "+ alarm + (alarmDeleted ? "" : " NOT")  + " Deleted"

            }
            else if (networkService.nodeExists(id))
            {
                Boolean alarmDeleted = networkService.removeAlarmFromNode(id, alarm)
                return "Node "+ alarmType.toString() + (alarmDeleted ? "" : " NOT")  + " Deleted"
            }
        }

        return ("Item with the Identifier: " + id +  " does NOT exist on Network!")


    }
    /***********************************************************************************  ALARM OPERATIONS SECTION   **********************************/

    @GetMapping("/getAlarms")
     String getNetworkAlarms() {

        List<NetworkHub> hubs = networkService.getAllHubsWithAlarms()
        List<NetworkNode> nodes = networkService.getAllNodesWithAlarms()
        if (hubs.isEmpty() && nodes.isEmpty()) {
            return " ****** No Alarms found on Network. ****** "
        } else {
            String info = "****** Hubs with Alarms : " + System.getProperty("line.separator")
            for (NetworkHub hub : hubs) {
                info += "  " + hub.getHubName() + "*** Alarm Number on Hub: " + hub.getHubAlarms().size() +
                        " *** Alarm Type: " + hub.getHubAlarms().toString()
            }
            info += System.getProperty('line.separator')+"  ****** Nodes with Alarms : " + System.getProperty("line.separator")
            for (NetworkNode node : nodes) {
                info += "  " + node.getNodeName() + " *** Alarm Count on Node: " + node.getNodeAlarms().size() +
                        " *** Alarm Type: " + node.getNodeAlarms().toString() + " ******"

            }
            return info
        }
    }

    @GetMapping("/getHubAlarms/{hubId}")
     String getHubAlarmsByHubId(@PathVariable int hubId) {
        String info
        try {
            if (networkService.hubExists(hubId)) {
                List<NetworkAlarm> hubsAlarms = networkService.getHubAlarms(hubId)

                if (hubsAlarms.isEmpty()) {
                    info = "****** No alarms on selected hub with id: " + hubId
                } else {
                    info = "****** Alarms on Hub: " + hubsAlarms.size() + "  " + System.getProperty("line.separator")
                    info += hubsAlarms.toString()
                }
            } else {
                info = " No Hub with Id = " + hubId

            }
        } catch (Exception ex)
        {
            return "Something is wrong with hub id provided " + ex
        }
        return info
    }

    @GetMapping("/getNodeAlarms/{nodeId}")
     String getNodeAlarmsByNodeId(@PathVariable int nodeId) {
        String info = new String()

        try {
            if (networkService.nodeExists(nodeId)) {
                List<NetworkAlarm> nodeAlarms = networkService.getNodeAlarms(nodeId)

                if (nodeAlarms.isEmpty()) {
                    info = "****** No alarms on selected node with id: " + nodeId
                } else {
                    info = "****** Alarms on Node: " + nodeAlarms.size() + System.getProperty("line.separator")
                    info += nodeAlarms.toString()
                }
            } else {
                info = "No Node exists with id " + nodeId
            }
        } catch (Exception ex) {
            return "Something is wrong with node id provided " + ex
        }
        return info
    }

    @GetMapping("/getNodeAlarmRemedies/{nodeId}")
     String getNodeAlarmsRemediesByNodeId(@PathVariable int nodeId) {
        String info = new String()

        try {
            if (networkService.nodeExists(nodeId)) {
                List<NetworkAlarm> nodeAlarms = networkService.getNodeAlarms(nodeId)

                if (nodeAlarms.isEmpty())
                {
                    info = "****** No alarms on selected node with id: " + nodeId
                } else {
                    List<AlarmRemedyTypeEnum> remedies = new ArrayList<>()
                    for(NetworkAlarm alarm : nodeAlarms)
                    {
                        info +=  " *** Alarm Status: " + (alarm.isActive() ? "active " : "not active ")+ " *** Alarm Remedy: " + alarm.getAlarmRemedy()  + " *** "
                    }
                }
            } else {
                info = "No Node exists with id: " + nodeId
            }
        } catch (Exception ex) {
            return "Something is wrong with node id provided " + ex
        }
        return info

    }
    @GetMapping("/getHubAlarmRemedies/{hubId}")
     String getHubAlarmsRemediesByHubId(@PathVariable int hubId) {
        String info = new String()

        try {
            if (networkService.hubExists(hubId)) {
                List<NetworkAlarm> hubAlarms = networkService.getHubAlarms(hubId)

                if (hubAlarms.isEmpty()) {
                    info = "****** No alarms on selected node with id: " + hubId
                } else
                {
                    List<AlarmRemedyTypeEnum> remedies = new ArrayList<>()

                    for (NetworkAlarm alarm : hubAlarms)
                    {
                        info += " *** Alarm Status: " + (alarm.isActive() ? "active " : "not active ")  + "Alarm Remedy: " + alarm.getAlarmRemedy() + " *** "
                    }
                }

                HashSet<NetworkNode> hubNodes = networkService.getAllNodesByHubId(hubId)
                if (!hubNodes.isEmpty())
                {
                    for (NetworkNode node : hubNodes)
                    {
                        if (!node.getNodeAlarms().isEmpty())
                        {
                            info += " *** Alarms on  Node Id: " + node.getNodeId() + " " + node.getNodeAlarms() + " *** "
                        }
                        else
                        {
                            info += " *** No Alarms on Node " + node.getNodeId() + " *** "
                        }
                    }
                }
                else
                {
                    info = " *** No Node exists with id" + hubId + " *** "
                }
            }
        }
        catch (Exception ex)
        {
            return "Something is wrong with node id provided " + ex
        }
        return info

    }


    @PostMapping("/addAlarmToNetwork/{networkIdentifier}/properties")
    String addAlarmToNetworkElementId(@PathVariable int networkIdentifier, @RequestBody Map<String, String>  newAlarm)
    {
        if (networkService.identifierExistsOnNetwork(networkIdentifier))
        {
            if (networkService.hubExists(networkIdentifier))
            {
                Boolean alarmCreated = networkService.addAlarmToHubId(networkIdentifier, newAlarm['alarmType'])

                return "Hub Alarm"+ (alarmCreated ? "" : " NOT")  + " Created"
            }
            else if (networkService.nodeExists(networkIdentifier))
            {
                Boolean alarmCreated = networkService.addAlarmToNodeId(networkIdentifier, newAlarm['alarmType'])

                return "Node Alarm"+ (alarmCreated ? "" : " NOT")  + " Created"
            }
        }
        return "Item with the Identifier: " + networkIdentifier +  "  does NOT exist on Network!"
    }

}
