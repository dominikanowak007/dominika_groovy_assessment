package com.example.dnowak_groovy.controlservice

import com.example.dnowak_groovy.networkbeans.AlarmTypeEnum
import com.example.dnowak_groovy.networkbeans.NetworkAlarm
import com.example.dnowak_groovy.networkbeans.NetworkCarrier
import com.example.dnowak_groovy.networkbeans.NetworkHub
import com.example.dnowak_groovy.networkbeans.NetworkNode
import org.springframework.stereotype.Service


@Service
class NetworkService {


    private static ArrayList<NetworkCarrier> network = []

    static
    {
        ArrayList<NetworkAlarm> hubAlarmsList = []
        NetworkAlarm alarm1= new NetworkAlarm( AlarmTypeEnum.DARK_FIBRE)
        hubAlarmsList.add(alarm1)
        hubAlarmsList.add(new NetworkAlarm(AlarmTypeEnum.UNIT_UNAVAILABLE))
        ArrayList<NetworkAlarm> nodeAlarmList = []
        NetworkAlarm alarm2 = new NetworkAlarm(  AlarmTypeEnum.DARK_FIBRE)
        nodeAlarmList.add(alarm2)
        nodeAlarmList.add(new NetworkAlarm(AlarmTypeEnum.DARK_FIBRE))
        nodeAlarmList.add(new NetworkAlarm( AlarmTypeEnum.OPTICAL_LOSS))
        def firstNode = new NetworkNode(nodeName: "FirstNode", nodeId: 11, nodeAlarms: nodeAlarmList)
        def secondNode = new NetworkNode(nodeName: "SecondNode", nodeId: 22)
        def thirdNode = new NetworkNode(nodeName: "FirstNode", nodeId: 23)
        Set<NetworkNode> nodeSet1 = [firstNode, secondNode]
        Set<NetworkNode> nodeSet2 = [thirdNode]
        def firstHub = new NetworkHub(hubName: 'FirstHub', hubId: 1, hubNodes: nodeSet1, hubAlarms: hubAlarmsList)
        def secondHub = new NetworkHub(hubName: 'SecondHub', hubId: 2, hubNodes: nodeSet2)
        def thirdHub = new NetworkHub(hubName: 'FirstHub', hubId: 3)
        Set<NetworkHub> hubSet1 = [firstHub, secondHub]
        Set<NetworkHub> hubSet2 = [thirdHub]
        def firstNetworkCarrier = new NetworkCarrier(carrierName: 'FirstCarrier', carrierHubs: hubSet1)
        def secondNetworkCarrier = new NetworkCarrier(carrierName: 'SecondCarrier', carrierHubs: hubSet2)

        network = [firstNetworkCarrier, secondNetworkCarrier]


    }

    static String getCurrentNetworkList() {
        return network?.toString()

    }

    String getAllCarriersNames() {
        List names = []
        network.each { it -> names.add(it.getCarrierName()) }
        return names
    }

    Set<NetworkHub> getAllHubsForCarrier(String carrierName) {

        NetworkCarrier searchedCarrier = network.find { it -> it.getCarrierName() == carrierName }
        Set<NetworkHub> hubs = searchedCarrier.getCarrierHubs()
        return hubs
    }


    List<NetworkNode> getAllNodesByHubId(int hubId) {
        Set<NetworkNode> nodes = []

        network.each { it ->
            it.getCarrierHubs().each { hub ->
                if (hub.getHubId() == hubId) {
                    nodes = hub.getHubNodes()
                }
            }
        }

        return nodes.toList()
    }

    NetworkNode getNodeByNetworkId(int nodeId) {
        NetworkNode singleNode
        network.each { carrier ->
            carrier.getCarrierHubs().each { hub ->
                hub.getHubNodes().each { node ->
                    if (node.getNodeId() == nodeId) {
                        singleNode = node
                    }
                }
            }
        }


        return singleNode

    }

    List getNodebyName(String nodeName) {

        List<NetworkNode> searchedNodes = new ArrayList<>()
        network.each { carrier ->
            carrier.getCarrierHubs().each { hub ->
                hub.getHubNodes().each { node ->
                    if (node.getNodeName() == nodeName) {
                        searchedNodes.add(node)
                    }
                }
            }
        }

        return searchedNodes

    }

    List getHubByName(String hubName) {

        List<NetworkHub> searchedHubs = []
        network.each { carrier ->
            carrier.getCarrierHubs().each { hub ->
                if (hub.getHubName() == hubName) {
                    searchedHubs.add(hub)
                }
            }
        }

        return searchedHubs
    }

    boolean nodeExists(int nodeId) {
        boolean nodeExists = false
        network.each { carrier -> carrier.getCarrierHubs().each { hub -> hub.getHubNodes().each { node -> if (node.getNodeId() == nodeId) nodeExists = true } } }
        return nodeExists
    }

    boolean nodeExists(String nodeName) {
        boolean nodeExists = false
        def nn = network.each { carrier -> carrier.getCarrierHubs().each { hub -> hub.getHubNodes().each { node -> if (node.getNodeName() == nodeName) nodeExists = true } } }

        return nodeExists
    }

    boolean nodeExistsOnHub(int hubId, String nodeName) {

        boolean exists = false
        network.each { carrier ->
            carrier.getCarrierHubs().each { hub ->
                if (hub.getHubId() == hubId) {
                    hub.getHubNodes().each { node -> if (node.getNodeName() == nodeName) exists = true }
                }
            }
        }
        return exists
    }

    boolean hubExists(int hubId) {
        def exists = false
        network.each { carrier ->
            carrier.getCarrierHubs().each { hub ->
                if (hub.getHubId() == hubId) {
                    exists = true
                }
            }
        }
        return exists
    }

    boolean identifierExistsOnNetwork(int identifier) {
        return hubExists(identifier) || nodeExists(identifier)
    }

    NetworkNode addNodeToHubId(int hubId, String newNodeName, int newNodeId) {
        NetworkHub searchedHub

        network.each { carrier ->
            carrier.getCarrierHubs().each { hub ->
                if (hub.getHubId() == hubId) {
                    searchedHub = hub
                }
            }
        }
        NetworkNode newNode = new NetworkNode(newNodeName, newNodeId)

        searchedHub.addNodetoHub(newNode)

        return newNode
    }

    public NetworkHub addHubToNamedCarrier(String carrierName, String hubName, int hubId) {
        NetworkCarrier searchedCarrier
        network.each { carrier ->
            if (carrier.getCarrierName() == carrierName) {
                searchedCarrier = carrier
            }
        }

        NetworkHub newHub = new NetworkHub(hubName: hubName, hubId: hubId)
        searchedCarrier.addHubToCarrier(newHub)

        return newHub
    }

    boolean carrierExistsOnNetwork(String name) {
        return getAllCarriersNames().contains(name)
    }

    boolean hubExistsforCarrier(String carrierName, String hubName) {
        network.each { carrier -> if (carrier.getCarrierName() == carrierName) carrier.getCarrierHubs().each { hub -> if (hub.getHubName() == hubName) return true } }
        return false
    }

    NetworkCarrier addCarrierToNetwork(String carrierName) {

        NetworkCarrier newCarrier = new NetworkCarrier(carrierName: carrierName)
        network.add(newCarrier)

        return newCarrier

    }

    NetworkHub getNetworkHubById(int id) {
        NetworkHub searchedHub

        network.each { carrier ->
            carrier.getCarrierHubs().each { hub ->
                if (hub.getHubId() == id) {
                    searchedHub = hub
                }
            }
        }

        return searchedHub
    }

    int addAlarmtoNetworkElement(int networkIdentifier, NetworkAlarm newAlarm) {
        if (hubExists(networkIdentifier)) {
            NetworkHub hub = getNetworkHubById(networkIdentifier)
            hub.addAlarmtoHub(newAlarm.getAlarmType())
            if (newAlarm.getAlarmType() == AlarmTypeEnum.UNIT_UNAVAILABLE) {
                List<NetworkNode> hubNodes = getAllNodesByHubId(networkIdentifier)
                for (NetworkNode n in hubNodes) {
                    n.addNodeAlarm(newAlarm.getAlarmType())
                }
            }
            return 1
        } else if (nodeExists(networkIdentifier)) {
            NetworkNode node = getNodeByNetworkId(networkIdentifier)
            node.addNodeAlarm(newAlarm.getAlarmType())
            return 2
        }
        return 0

    }

    NetworkCarrier getNetworkCarrierByName(String name) {
        NetworkCarrier carrier
        network.each { c -> if (c.getCarrierName() == name) carrier = c }
        return carrier
    }

    boolean updateCarrierName(String currentName, String newName) {

        if (!carrierExistsOnNetwork(newName)) {
            NetworkCarrier existingCarrier = getNetworkCarrierByName(currentName)
            existingCarrier.setCarrierName(newName)
            return true
        } else false
    }

    Boolean updateExistingHub(int id, String newHubName) {
        Set<NetworkHub> siblings = null
        boolean hubNameExistsforCarrier = false
        network.each { carrier ->
            carrier.getCarrierHubs().each { hub ->
                if (hub.getHubId() == id) {
                    siblings = carrier.getCarrierHubs()
                }
            }
        }
        siblings.each { hub -> if (hub.getHubName() == newHubName) hubNameExistsforCarrier = true }

        if (!hubNameExistsforCarrier) {
            NetworkHub hub = getNetworkHubById(id)
            hub.setHubName(newHubName)
            return true

        } else false
    }

    boolean updateNodeName(Map<String, String> propertiesMap) {
        Set<NetworkNode> siblings = null
        boolean nodeNameExistForHub = false
        int nodeId = propertiesMap['nodeId'].toInteger()
        String nodeNewName = propertiesMap['nodeName']
        network.each { carrier ->
            carrier.getCarrierHubs().each { hub ->
                hub.getHubNodes().each { node ->
                    if (node.getNodeId() == nodeId) {
                        siblings = hub.getHubNodes()
                    }
                }
            }
        }
        siblings.each { node -> if (node.getNodeName() == nodeNewName) nodeNameExistForHub = true }

        if (!nodeNameExistForHub) {
            NetworkNode node = getNodeByNetworkId(nodeId)
            node.setNodeName(nodeNewName)
            return true
        } else false
    }

    boolean deleteHubFromNetwork(int id) {
        NetworkHub hubToBeRemoved = getNetworkHubById(id)
        NetworkCarrier hubCarrier
        network.each { carrier ->
            carrier.getCarrierHubs().each { hub ->
                if (hub.getHubId() == id) {
                    hubCarrier = carrier
                }
            }
        }
        return hubCarrier.removeHubfromCarrier(hubToBeRemoved)

    }

    boolean deleteNodeFromNetwork(int id) {
        NetworkNode nodeToBeRemoved = getNodeByNetworkId(id)
        NetworkHub nodeHub
        network.each { carrier ->
            carrier.getCarrierHubs().each { hub ->
                hub.getHubNodes().each { node ->
                    if (node.getNodeId() == id) {
                        nodeHub = hub
                    }
                }
            }
        }
        return nodeHub.removeNodefromHub(nodeToBeRemoved)


    }

    Boolean deleteCarrier(String name) {
        NetworkCarrier carrierToBeRemoved = getNetworkCarrierByName(name)
        return network.each { carrier -> if (carrier.getCarrierName() == name) network.remove(carrierToBeRemoved) }
    }

    Boolean removeAllAlarmsFromHub(int id) {
        NetworkHub hub = getNetworkHubById(id)
        if (hub.getHubAlarms()) {
            hub.getHubAlarms()?.clear()
            return true
        } else false
    }

    Boolean removeAllAlarmsFromNode(int id) {
        NetworkNode node = getNodeByNetworkId(id)
        if (node.getNodeAlarms()) {
            node.getNodeAlarms()?.clear()
            return true
        } else false
    }

    Boolean removeAlarmFromHub(int id, AlarmTypeEnum alarmType) {
        NetworkHub hub = getNetworkHubById(id)
        def alarmOnHub = hub.getHubAlarms()
        boolean alarmExists = false
        alarmOnHub.each { a ->
            if (a.getAlarmType() == alarmType) {
                alarmExists = true
            }
        }
        if (alarmExists) {
            hub.removeAlarmfromHub(alarmType)
            return true
        }
        return false
    }

    Boolean removeAlarmFromNode(int id, AlarmTypeEnum alarmType) {
        NetworkNode node = getNodeByNetworkId(id)
        boolean alarmExistOnNode = false
        def alarmsOnNode = node.getNodeAlarms()
        alarmsOnNode.each { a ->
            if (a == alarmType) {
                alarmExistOnNode = true
            }
        }
        if (alarmExistOnNode) {
            node.removeNodeAlarm(new NetworkAlarm(alarmType: alarmType))
            return true
        } else false

    }


   List<NetworkHub> getAllHubsWithAlarms()
   {
    List<NetworkHub> hubsWithAlarms=[]

       network.each { carrier ->
           carrier.getCarrierHubs().each{ hub ->
               if (!hub.getHubAlarms().isEmpty()){hubsWithAlarms.add(hub)
               }
           }
       }
       return hubsWithAlarms
   }
    List<NetworkNode> getAllNodesWithAlarms()
    {
    List<NetworkNode> nodesWithAlarms=[]

    network.each { carrier ->
        carrier.getCarrierHubs().each { hub ->
            hub.getHubNodes().each{node->if(!node.getNodeAlarms().isEmpty()){nodesWithAlarms.add(node)}
            }
        }
    }
    return nodesWithAlarms
}

    List<NetworkAlarm> getHubAlarms(int id)
    {
        NetworkHub hub = getNetworkHubById(id)

        return hub.getHubAlarms().toList()
    }

    List<NetworkAlarm> getNodeAlarms(int id)
    {
        NetworkNode node= getNodeByNetworkId(id)
        return node.getNodeAlarms().toList()
    }

    boolean addAlarmToHubId(int id, String alarmType)
    {
        AlarmTypeEnum alarmTypeEnum = AlarmTypeEnum.valueOf(alarmType)

        NetworkHub hub = getNetworkHubById(id)
        NetworkAlarm alarm= new NetworkAlarm(alarmTypeEnum)

        return hub.addAlarmtoHub(alarmTypeEnum)
    }
    boolean addAlarmToNodeId(int id, String alarmType)
    {
        AlarmTypeEnum alarmTypeEnum = AlarmTypeEnum.valueOf(alarmType)
        NetworkNode node = getNodeByNetworkId(id)
        NetworkAlarm alarm= new NetworkAlarm(alarmTypeEnum)
         node.addNodeAlarm(alarmTypeEnum)
    }
}