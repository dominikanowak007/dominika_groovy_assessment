package com.example.dnowak_groovy

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class DnowakGroovyApplication {

	static void main(String[] args) {
		SpringApplication.run DnowakGroovyApplication, args
		print(
				"*******************" + System.lineSeparator() +
						"*******************" + " Hello there " + "*******************" +
						System.lineSeparator() + "*******************");
	}
}
