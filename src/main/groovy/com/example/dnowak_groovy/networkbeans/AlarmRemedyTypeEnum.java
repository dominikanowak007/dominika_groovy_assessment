package com.example.dnowak_groovy.networkbeans;

public enum AlarmRemedyTypeEnum
{
   AVAILABILITY_FIX,
   OPTICAL_LOSS_FIX,
   DARK_FIBRE_FIX,
   SWITCH_ON;

    AlarmRemedyTypeEnum() { }

    @Override
    public String toString() {
        return this.name();
    }
}
