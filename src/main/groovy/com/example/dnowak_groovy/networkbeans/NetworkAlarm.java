package com.example.dnowak_groovy.networkbeans;

public class NetworkAlarm {

    private AlarmTypeEnum alarmType;
    private boolean active;
    private AlarmRemedyTypeEnum alarmRemedy;

    public AlarmTypeEnum getAlarmType() {
        return alarmType;
    }
 //   public NetworkAlarm(){}



    public boolean isActive() {
        return active;
    }

    @Override
    public String toString() {
        return "NetworkAlarm{" +
                "alarmType = " + alarmType +
                ", active ? " + active +
                ", alarmRemedy = " + alarmRemedy +
                '}';
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public AlarmRemedyTypeEnum getAlarmRemedy() {
        return alarmRemedy;
    }

    public NetworkAlarm(AlarmTypeEnum alarmType)
    {
        this.alarmType = alarmType;
        this.active = true;

        if (alarmType == AlarmTypeEnum.DARK_FIBRE) {
            alarmRemedy = AlarmRemedyTypeEnum.DARK_FIBRE_FIX;
        }
        switch (alarmType) {
            case DARK_FIBRE:
                alarmRemedy = AlarmRemedyTypeEnum.DARK_FIBRE_FIX;
                break;
            case UNIT_UNAVAILABLE:
                alarmRemedy = AlarmRemedyTypeEnum.AVAILABILITY_FIX;
                break;
            case POWER_OUTAGE:
                alarmRemedy = AlarmRemedyTypeEnum.SWITCH_ON;
                break;
            case OPTICAL_LOSS:
                alarmRemedy = AlarmRemedyTypeEnum.OPTICAL_LOSS_FIX;
                break;
            default:
                break;
        }
    }
}
