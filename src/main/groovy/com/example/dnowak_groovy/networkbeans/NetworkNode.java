package com.example.dnowak_groovy.networkbeans;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class NetworkNode  implements Comparable<NetworkNode>{

    @Override
    public String toString() {
        return "NetworkNode{" + "\r\n" +
                " Node Name = '" + nodeName + "\r\n" +
                ", Node Id = " + nodeId + "\r\n" +
                ", Node Alarms = " + (nodeAlarms.isEmpty() ? " No Node Alarms " : getNodeAlarms().toString()) + "\r\n" +
                '}';
    }

    private String nodeName;
    private int nodeId;

    List<NetworkAlarm> nodeAlarms = new ArrayList<>();

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }
    public NetworkNode() {}

    public NetworkNode(String nodeName, int nodeId) {
        this.nodeName = nodeName;
        this.nodeId = nodeId;

    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public List<NetworkAlarm> getNodeAlarms() {
        return nodeAlarms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NetworkNode)) return false;
        NetworkNode that = (NetworkNode) o;

        return nodeId == that.nodeId &&
                Objects.equals(nodeName, that.nodeName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(nodeName, nodeId);
    }

    public Boolean alarmExists( AlarmTypeEnum nodeAlarm)
    {

        return nodeAlarms.stream().anyMatch(na->na.getAlarmType()==nodeAlarm);
    }

    public Boolean addNodeAlarm(AlarmTypeEnum alarm)
    {
        return nodeAlarms.add(new NetworkAlarm(alarm));
    }

    public Boolean removeNodeAlarm(NetworkAlarm alarm)
    {
          return  nodeAlarms.add(alarm);

    }

    public Boolean removeAllNodeAlarms()
    {
        if (nodeAlarms.isEmpty())
            return false;
        else {
            nodeAlarms.clear();
            return true;
        }
    }

    @Override
    public int compareTo(NetworkNode o) {
        return this.getNodeId() - o.getNodeId();
    }
}
