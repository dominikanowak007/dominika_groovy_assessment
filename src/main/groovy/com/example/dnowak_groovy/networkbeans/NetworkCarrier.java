package com.example.dnowak_groovy.networkbeans;


import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class NetworkCarrier implements Comparable<NetworkCarrier>{


    private String carrierName;
    private HashSet<NetworkHub> carrierHubs= new HashSet<>();

    public NetworkCarrier( String carrierName) {

        this.carrierName = carrierName;
    }

    public NetworkCarrier(){}

    @Override
    public String toString() {

        List hubsSortedList = new ArrayList(carrierHubs);
        Collections.sort(hubsSortedList);

        return  " Network Carrier{" + System.getProperty("line.separator") +
                " Carrier Name = " + carrierName + '\r'  +
                ", Carrier Hubs = " + (carrierHubs.isEmpty() ? " No Hubs " : hubsSortedList.toString())  +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof NetworkCarrier)) return false;
        NetworkCarrier that = (NetworkCarrier) o;
        return Objects.equals(carrierName, that.carrierName);
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(carrierName);
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public HashSet<NetworkHub> getCarrierHubs() {
        return carrierHubs;
    }

    public void setCarrierHubs(HashSet<NetworkHub> carrierHubs) {
        this.carrierHubs = carrierHubs;
    }


    public Boolean addHubToCarrier(NetworkHub networkHub) {

            return carrierHubs.add(networkHub);
    }

    public Boolean removeHubfromCarrier(NetworkHub hub)
    {
       return carrierHubs.remove(hub);

    }

    public Boolean hubExists(int id)
    {
       return carrierHubs.stream().anyMatch(p->p.getHubId()==id);
    }


    public int compareTo(NetworkCarrier carrier) {

        return this.getCarrierName().compareTo(carrier.getCarrierName());
    }

    public Boolean anyCarrierAlarmsExists()
    {
        Predicate<NetworkNode> activeNodeAlarms = e->(e.getNodeAlarms().size() > 0);
        List<NetworkNode> nodeswithAlarms =  this.getCarrierHubs().stream().flatMap(h->h.getHubNodes().stream()).filter(activeNodeAlarms).collect(Collectors.toList());

        return  this.getCarrierHubs().stream().anyMatch(h->h.getHubAlarms().isEmpty()) || nodeswithAlarms.size() > 0;
    }

    public static Comparator<NetworkCarrier> NetworkCarrierComparator = new Comparator<NetworkCarrier>()
    {

        public int compare(NetworkCarrier carrier1, NetworkCarrier carrier2)
        {
            String name1 = carrier1.getCarrierName();
            String name2 = carrier2.getCarrierName();
            return name1.compareTo(name2);

        }
    };
}
