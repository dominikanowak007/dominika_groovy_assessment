package com.example.dnowak_groovy.networkbeans;



import java.util.*;

public class NetworkHub  implements Comparable<NetworkHub>{

    private String hubName;

    private int hubId;

    private HashSet<NetworkNode> hubNodes = new HashSet<>();
     ArrayList<NetworkAlarm> hubAlarms = new ArrayList<>();

    public String getHubName() {
        return hubName;
    }

    @Override
    public String toString() {

        List nodesSortedList = new ArrayList(hubNodes);
        Collections.sort(nodesSortedList);

        return "NetworkHub\n" + '\n' +
                "Hub Name = '" + hubName + '\n' +
                ", Hub Id = " + hubId + '\n' +
                ", Hub Nodes = "  + (hubNodes.isEmpty() ? " No Nodes " : nodesSortedList)+'\n' +
                ", Hub Alarms = " + (hubAlarms.isEmpty() ? " No Hub Alarms " : getHubAlarms().toString()) + '\n' +
                '}';
    }

    public void setHubName(String hubName) {
        this.hubName = hubName;
    }

    public int getHubId() {
        return hubId;
    }

    public void setHubId(int hubId) {
        this.hubId = hubId;
    }

    public HashSet<NetworkNode> getHubNodes() {
        return hubNodes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NetworkHub)) return false;
        NetworkHub that = (NetworkHub) o;
        return hubId == that.hubId &&
                Objects.equals(hubName, that.hubName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(hubName, hubId);
    }

    public void setHubNodes(HashSet<NetworkNode> hubNodes) {
        this.hubNodes = hubNodes;
    }

    public ArrayList<NetworkAlarm> getHubAlarms()
    {
        return hubAlarms;
    }
    public Boolean alarmExists(AlarmTypeEnum alarmType)
    {
        return  getHubAlarms().stream().anyMatch(na->na.getAlarmType()==alarmType);
    }


    public void setHubAlarms(ArrayList<NetworkAlarm> hubAlarms) {
        this.hubAlarms = hubAlarms;
    }


    public NetworkHub(String hubName, int hubId) {
        this.hubName = hubName;
        this.hubId = hubId;

    }
    public NetworkHub(){}

    public Boolean addNodetoHub(NetworkNode node)
    {
         return hubNodes.add(node);
    }

    public Boolean addAlarmtoHub(AlarmTypeEnum alarmType)
    {
       if(alarmType.equals(AlarmTypeEnum.UNIT_UNAVAILABLE))
       {
           if (hubAlarms.add(new NetworkAlarm(alarmType)))
           {
               hubNodes.forEach(networkNode -> networkNode.addNodeAlarm(alarmType));
               return true;
           }
           else
               return false;
       }
        return hubAlarms.add(new NetworkAlarm(alarmType));
    }

    public Boolean removeAlarmfromHub(AlarmTypeEnum alarmType)
    {
        return hubAlarms.remove(alarmType);

    }
    public Boolean removeNodefromHub(NetworkNode node)
    {
            return hubNodes.remove(node);

    }
    private Boolean removeAllHubAlarms()
    {
        if (hubAlarms.isEmpty())
            return false;
        else {
            hubAlarms.clear();
            return true;
        }
    }

    public void removeAllExistingHubAndNodesAlarms()
    {
        hubNodes.forEach(networkNode -> networkNode.nodeAlarms.clear());
        removeAllHubAlarms();
    }


    @Override
    public int compareTo(NetworkHub o)
    {
        return this.getHubId() - o.getHubId();
    }
}
