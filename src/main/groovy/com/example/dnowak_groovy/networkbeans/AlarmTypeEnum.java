package com.example.dnowak_groovy.networkbeans;

public enum AlarmTypeEnum
{
    UNIT_UNAVAILABLE ,
    OPTICAL_LOSS ,
    DARK_FIBRE ,
    POWER_OUTAGE;


    AlarmTypeEnum()
    {
  }

}
